<?php
namespace Shin;

use Symfony\Component\Yaml\Yaml;

class ServerRunner
{
    private $host;
    private $port;
    private $root;
    private $routing;
    private $ini_dir;
    private $define;
    private $yaml;
    private $command;
    private $commands;
    private $exec;

    public function __construct()
    {
        if (php_sapi_name()==='cli' or defined('STDIN')) {
            if (!FILE_EXISTS("dev_server.yaml")) {
                ServerInit::setupWizard();
            }
            
            $this->yaml = Yaml::parse(file_get_contents("dev_server.yaml"));
            $this->exec = new CommandExec();
            $this->evalConf();
        } else {
            die("<pre>please RUN this program from command line interface (CLI)</pre>");
        }
    }
    private function evalConf()
    {
        $yml = $this->yaml;
        foreach ($yml as $property => $value) {
            if (property_exists($this, $property)) :
                $this->$property = $value;
            endif;
        }
        $this->setCommand();
    }
    private function setCommand()
    {
        $this->commands = PHP_BINARY." -S ".$this->host.":".$this->port;
        if (null!==$this->root) {
            $this->commands .=" -t ".$this->root;
        }
        if (null!==$this->define) {
            $this->commands .=" -d ".implode(" -d ", $this->define);
        }
        if (null!==$this->ini_dir) {
            $this->commands .=" -c ".$this->ini_dir;
        }
        if (null!==$this->routing) {
            $this->commands .=" ".$this->routing;
        }
        if (null!==$this->command) {
            foreach ($this->command as $cmd) {
                $this->exec->add($cmd);
            }
        }
        $this->exec->add("echo '> Running PHP development server powered by shin <fb.me/shiin.kun2>.'");
        $this->exec->add($this->commands);
    }
    public function run()
    {
        $this->exec->run();
    }
}
