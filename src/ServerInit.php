<?php
namespace Shin;

use Zend\Console\Prompt\Line;
use Symfony\Component\Yaml\Yaml;

class ServerInit
{

    final public static function setupWizard()
    {
        $data = [];
        $name = Line::prompt(
            'server name (required):',
            false,
            100
        );
        $data['name'] = $name;
        $host = Line::prompt(
            'server host (127.0.0.1):',
            true,
            100
        );
        $data['host'] = empty($host)?"127.0.0.1":$host;
        $port = Line::prompt(
            'server port (8080):',
            true,
            100
        );
        $data['port'] = empty($port)?8080:(int)$port;
        $root = Line::prompt(
            'server root (optional):',
            true,
            100
        );
        if (!empty($root)) {
            if (substr($root, -1)!="/") {
                $data["root"] = $root;
            } else {
                $data["root"] = substr($root, 0, -1);
            }
        }
        $routing = Line::prompt(
            'server routing (optional):',
            true,
            100
        );
        if (!empty($routing)) {
            if ($routing == "internal") {
                if (empty($root)) {
                    file_put_contents('server_routing.php', RouteTemplate::getDefault());
                    $data["routing"] = "server_routing.php";
                } else {
                    file_put_contents($root.'/server_routing.php', RouteTemplate::getDefault());
                    $data["routing"] = $root."/server_routing.php";
                }
            } else {
                $data["routing"] = $routing;
            }
        }
        $count = 1;
        while (true) {
            $define = Line::prompt(
                'Define INI entry '.$count.' (optional):',
                true,
                100
            );
            if (!empty($define)) {
                $data['define'][] = $define;
                $count +=1;
            } else {
                $count = 1;
                break;
            }
        }
               $ini_dir = Line::prompt(
                   'Look for php.ini file in this directory (optional):',
                   true,
                   100
               );
        if (!empty($ini_dir)) {
            $data["ini_dir"] = $ini_dir;
        }
        while (true) {
            $command = Line::prompt(
                'command '.$count.' (optional):',
                true,
                100
            );
            if (!empty($command)) {
                $data['command'][] = $command;
                $count +=1;
            } else {
                break;
            }
        }
        $yaml = Yaml::dump($data);
        file_put_contents('dev_server.yaml', $yaml);
    }
}
