<?php
namespace Shin;

class CommandExec
{
    private $command;
    
    public function __construct()
    {
        $this->command = new \SplQueue();
    }
    public function add($command)
    {
        $this->command->enqueue($command);
    }
    public function run()
    {
        while ($this->command->isEmpty()!=true) {
            $this->command->rewind();
            passthru($this->command->current());
            $this->command->dequeue();
            sleep(1);
        }
    }
}
